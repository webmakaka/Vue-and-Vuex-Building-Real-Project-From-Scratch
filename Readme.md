# [Udemy, Oleksandr Kocherhin] Vue и Vuex - пишем реальный проект с нуля [RUS, 2020]

<br/>

**Пример проекта:**  
http://demo.realworld.io/

<br/>

**Оригинальные исходники:**  
https://github.com/gothinkster/realworld

<br/>

**Backend от Oleksandr:**  
https://github.com/EJIqpEP/koa-knex-realworld-example

<br/>

**Конфиги для NeoVim от Oleksandr Kocherhin**  
https://github.com/webmak1/nvim-config

<br/>

### [Разработка по шагам](./Development.md)

<br/>

---

<br/>

**Marley**

Any questions in english: <a href="https://jsdev.org/chat/">Telegram Chat</a>  
Любые вопросы на русском: <a href="https://jsdev.ru/chat/">Телеграм чат</a>
